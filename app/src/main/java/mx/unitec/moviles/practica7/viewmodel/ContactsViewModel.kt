package mx.unitec.moviles.practica7.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import mx.unitec.moviles.practica7.model.Contact
import mx.unitec.moviles.practica7.repository.ContactsRepository

class ContactsViewModel(application: Application) : AndroidViewModel(application) {
    private val repository = ContactsRepository(application)

    val contacts = repository.getContacts()

    fun saveContact(contact: Contact) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(contact)
    }
    fun deleteContact(contact: Contact)=viewModelScope.launch(Dispatchers.IO)
    {
        repository.delete(contact)
    }
}